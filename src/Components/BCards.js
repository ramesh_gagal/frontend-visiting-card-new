import React, { useEffect, useState } from 'react';
import { Card, Col, Row } from 'react-bootstrap';
import { MdEmail } from "react-icons/md";
import { FaPhoneAlt } from "react-icons/fa";
import ImageSwiper from '../Utils/ImageSwiper';
import axios from 'axios';
import { CiLinkedin } from "react-icons/ci";
import toast from 'react-hot-toast';

const BusinessCard = () => {
  const [state, setState] = useState([]);
  const ApiCall = async () => {
    try {
      const response = await axios.get("https://visiting-card-backend.onrender.com/api/v1/card/list");
      setState(response?.data?.data);
      toast.success("Card created successfully")
      console.log(response, "response");
      console.log(state, "staaaaaaaaaaaaaaa");
    } catch (e) {
      console.log(e, "error");
      toast.error("something went wrong")
    }
    
  };

  useEffect(() => {
    ApiCall();
  }, []);

  return (
    <Row xs={1} md={2} lg={4} className="g-4 mt-3">
      {state?.map((i, index) => (
        <Col key={index}>
          <Card className="bg-light" style={{height: "500px"}}>
            <Card.Header className='m-0 p-0' >
              <ImageSwiper images={i?.images} />
            </Card.Header>
            <Card.Body>
              <div className="info">
                <Card.Title className='text-left'>{i?.name}</Card.Title>
                <Card.Text style={{"text-align" : "left"}}>{i?.about}</Card.Text>
                <Card.Text className='d-flex align-items-center '>
                  <div className="d-flex align-items-center ">
                    <MdEmail size={18} className="email-icon bg-primary rounded-circle text-white me-2" />
                    <Card.Text>{i?.email}</Card.Text>
                  </div>
                </Card.Text>
                <Card.Text>
                  <div className="d-flex align-items-center ">
                    <FaPhoneAlt size={18} className="email-icon bg-primary rounded-circle text-white me-2" />
                    <Card.Text>{i?.phone}</Card.Text>
                  </div>
                </Card.Text>
                <Card.Text>
                  <div className="d-flex align-items-center">
                    <CiLinkedin size={18} className="email-icon bg-primary rounded-circle text-white me-2" />
                    <Card.Text ><span className='text-wrap'>{i?.linkedin}</span></Card.Text>
                  </div>
                </Card.Text>
              </div>
            </Card.Body>
          </Card>
        </Col>
      ))}
    </Row>
  );
};

export default BusinessCard;
