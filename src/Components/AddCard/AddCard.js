import React, { useState } from 'react';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { Button, Col, Container, Form as BootstrapForm, Row, Alert, Card, Spinner } from 'react-bootstrap';
import { FaEnvelope, FaPhone, FaLinkedin, FaUser, FaInfo, FaIdCard } from 'react-icons/fa';
import Crop from './Crop';
import axios from 'axios';
import { useNavigate } from 'react-router';

const AddCard = () => {
  const [loading, setloading] = useState(false)
const navigate = useNavigate()
  const initialValues = {
    name: '',
    email: '',
    phone: '',
    linkedin: '',
    about: '',
    title: '',
    image: null,
  };
  

  const validationSchema = Yup.object().shape({
    name: Yup.string().trim().required('Name is required'),
    email: Yup.string().trim().email('Invalid email address').required('Email is required'),
    phone: Yup.string().trim()
  .matches(/^[0-9]{10}$/, 'Please enter valid phone number')
  .required('Phone number is required'),

    linkedin: Yup.string().trim().url('Invalid LinkedIn URL').required('LinkedIn profile is required'),
    about: Yup.string().trim().max(200, 'About section should be at most 200 characters').required('About is required'),
    title: Yup.string().trim().max(50, 'Title should be at most 50 characters').required('Title is required'),
    image: Yup.mixed().required('Image is required'),

  });



  const onSubmit = async (values, { resetForm, setFieldValue })=> {
    // values['image'] = base64Array
    console.log('Form submitted:', values);
    values['image'] = values?.image?.map((i)=> i.base64URL)
    try {
        setloading(true)
      const response = await axios.post("https://visiting-card-backend.onrender.com/api/v1/card/create", values)
      navigate("/")
      console.log(response, "response")
    resetForm();

    } catch (error) {
      console.log(error, "weror")
    }finally{
      setloading(false)
    }
  };

  return (
    <Container >
      <Row className="justify-content-center">
        <Col xs={12} md={8} lg={6}>
          <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
            {({ setFieldValue, values }) => (
              <Form as={BootstrapForm} className="mt-4 p-4 rounded shadow" style={{ background: "#ffffff" }}>
                <h2 className="text-center mb-4">Create Your Business Card</h2>

                <BootstrapForm.Group controlId="name" className='my-2'>
                  <BootstrapForm.Label><FaUser className="me-2" />Name</BootstrapForm.Label>
                  <Field type="text" name="name" className="form-control" placeholder="John Doe" />
                  <ErrorMessage name="name" component="div" className="text-danger" />
                </BootstrapForm.Group>

                <BootstrapForm.Group controlId="email" className='my-2'>
                  <BootstrapForm.Label><FaEnvelope className="me-2" />Email</BootstrapForm.Label>
                  <Field type="email" name="email" className="form-control" placeholder="john@example.com" />
                  <ErrorMessage name="email" component="div" className="text-danger" />
                </BootstrapForm.Group>

                <BootstrapForm.Group controlId="phone" className='my-2'>
                  <BootstrapForm.Label><FaPhone className="me-2" />Phone</BootstrapForm.Label>
                  <Field type="text" name="phone" className="form-control" placeholder="1234567890" />
                  <ErrorMessage name="phone" component="div" className="text-danger" />
                </BootstrapForm.Group>

                <BootstrapForm.Group controlId="linkedin" className='my-2'>
                  <BootstrapForm.Label><FaLinkedin className="me-2" />LinkedIn</BootstrapForm.Label>
                  <Field type="text" name="linkedin" className="form-control" placeholder="https://linkedin.com/in/johndoe" />
                  <ErrorMessage name="linkedin" component="div" className="text-danger" />
                </BootstrapForm.Group>

                <BootstrapForm.Group controlId="about" className='my-2'>
                  <BootstrapForm.Label><FaInfo className="me-2" />About</BootstrapForm.Label>
                  <Field as="textarea" name="about" rows={3} className="form-control" placeholder="A brief introduction about yourself" />
                  <ErrorMessage name="about" component="div" className="text-danger" />
                </BootstrapForm.Group>

                <BootstrapForm.Group controlId="title" className='my-2' >
                  <BootstrapForm.Label><FaIdCard className="me-2" />Title</BootstrapForm.Label>
                  <Field type="text" name="title" className="form-control" placeholder="Software Engineer" />
                  <ErrorMessage name="title" component="div" className="text-danger" />
                </BootstrapForm.Group>

                <BootstrapForm.Group controlId="image" className='my-2' >
                  <BootstrapForm.Label><FaIdCard className="me-2" />Image</BootstrapForm.Label>
                  <Crop name="image" setFieldValue={setFieldValue} values={values}  />
                  {/* <Field type="text" name="image" className="form-control" placeholder="Software Engineer" /> */}
                  <ErrorMessage name="image" component="div" className="text-danger" />
                </BootstrapForm.Group>

                <Button type="submit" variant="primary" className="mt-3" disabled={loading}>
                  Create Business Card {loading && <Spinner   className="ms-1 text-light spinner-border-sm"
                          size='sm'
                        />}
                </Button>

                <Alert variant="info" className="mt-3">
                  Note: This is just a demonstration. The form does not submit anywhere.
                </Alert>
              </Form>
            )}
          </Formik>
        </Col>
      </Row>
    </Container>
  );
};

export default AddCard;
