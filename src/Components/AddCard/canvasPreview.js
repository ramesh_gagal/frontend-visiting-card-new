export async function canvasPreview(
  image,
  canvas,
  crop,
  scale = 1,
  rotate = 0,
  minCropSize = 10, // Adjust this value as needed
) {
  const ctx = canvas.getContext('2d');

  if (!ctx) {
    throw new Error('No 2d context');
  }

  const scaleX = image.naturalWidth / image.width;
  const scaleY = image.naturalHeight / image.height;
  const pixelRatio = window.devicePixelRatio;

  const canvasWidth = Math.floor(crop.width * scaleX * pixelRatio);
  const canvasHeight = Math.floor(crop.height * scaleY * pixelRatio);

  // Check if the canvas size is below the minimum threshold
  if (canvasWidth < minCropSize || canvasHeight < minCropSize) {
    throw new Error('Crop area is too small');
  }

  canvas.width = canvasWidth;
  canvas.height = canvasHeight;

  ctx.scale(pixelRatio, pixelRatio);
  ctx.imageSmoothingQuality = 'high';

  const cropX = crop.x * scaleX;
  const cropY = crop.y * scaleY;

  const rotateRads = rotate * (Math.PI / 180);
  const centerX = image.naturalWidth / 2;
  const centerY = image.naturalHeight / 2;

  ctx.save();

  ctx.translate(-cropX, -cropY);
  ctx.translate(centerX, centerY);
  ctx.rotate(rotateRads);
  ctx.scale(scale, scale);
  ctx.translate(-centerX, -centerY);
  ctx.drawImage(
    image,
    0,
    0,
    image.naturalWidth,
    image.naturalHeight,
    0,
    0,
    image.naturalWidth,
    image.naturalHeight,
  );

  ctx.restore();

  return new Promise((resolve, reject) => {
    canvas.toBlob(
      (blob) => {
        if (!blob) {
          reject(new Error('Could not create blob'));
          return;
        }

        const file = new File([blob], 'preview.jpg', {
          type: 'image/jpeg',
          lastModified: Date.now(),
        });

        resolve(file);
      },
      'image/jpeg',
      0.95,
    );
  });
}
