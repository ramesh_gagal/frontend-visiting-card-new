import React from 'react'
import Card from './Cards/Card'

const Layout = () => {
  return (
    <div className='container-fluid'>
      <div className='row'>
        <div className='col-12'>
           <Card />
        </div>
      </div>
    </div>
  )
}

export default Layout